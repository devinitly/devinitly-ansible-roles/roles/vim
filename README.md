VIM
=========

The VIM Ansible role installs VIM and vundle using the hosts OS respective Linux distribution.

Supported Distributions:
- Ubuntu
- CentOS

Requirements
------------

The following needs installed and configured to execue the role:
- Ansible
- python3
- git

Role Variables
--------------

Default Variables:
- owner: $USER
- group: $USER

Variables default to active user and group executing the playbook. Can be overidden using the supplied variables file.

Example Playbook
----------------
```yml
---
- hosts: localhost
  become: true
  connection: local
  gather_facts: yes
  vars:
    ansible_python_interpreter: /usr/bin/python3
  roles:
    - vim
```

License
-------

BSD

